﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Chemical
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = new StreamReader("input.txt");
            int C = int.Parse(reader.ReadLine());
            int H = int.Parse(reader.ReadLine());
            int O = int.Parse(reader.ReadLine());
            reader.Close();

            int Atom_C, Atom_H, Atom_O, result_C, result_H, result_O;
            Atom_C = 2;
            Atom_H = 6;
            Atom_O = 1;

            result_C =  C / Atom_C;
            result_H =  H / Atom_H;
            result_O =  O / Atom_O;

            if ((result_C <= result_H) && (result_C <= result_O))
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"{result_C}");
                writer.Close();
            }
            else if ((result_H <= result_C) && (result_H <= result_O))
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"{result_H}");
                writer.Close();
            }
            else if ((result_O <= result_H) && (result_O <= result_C))
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"{result_O}");
                writer.Close();
            }
            else
            {
                StreamWriter writer = new StreamWriter("output.txt");
                writer.WriteLine($"0");
                writer.Close();
            }


        }
    }
}
